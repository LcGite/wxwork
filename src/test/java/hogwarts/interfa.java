package hogwarts;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.HashMap;

import static io.restassured.RestAssured.given;



public class interfa {

    static String access_token;

    @Test
    void getToken() {
        access_token = given().log().all()
                .queryParam("corpid", "wwc9a59dd604b10c5f").queryParam("corpsecret", "IsIPuBjH-xSGM-rGBATzTOa9ln9JxxJUsGn82PqPLoQ")
                .get("https://qyapi.weixin.qq.com/cgi-bin/gettoken")
                .then()
                .log().body()
                .extract()
                .path("access_token");
    }


    @ParameterizedTest
    @ValueSource(strings = {"12345","hello","你好！"})
    @Test
        void sendMessage(String msg){

        HashMap<String,Object> body = new HashMap<String,Object>();
        body.put("touser","@all");
        body.put("toparty","");
        body.put("totag","");
        body.put("msgtype","text");
        body.put("agentid","1000002");
        HashMap<String,Object> content = new HashMap<String,Object>();
        content.put("content",msg);
        body.put("text",content);
        body.put("safe",0);

        given().queryParam("access_token",access_token)
        .contentType(ContentType.JSON)
        .body(body)
        .post("https://qyapi.weixin.qq.com/cgi-bin/message/send");
    }


}

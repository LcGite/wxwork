package com.wxwork.app;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class MemberPageTest {

    AppiumDriver<MobileElement> driver;

    @Test
    //添加成员->搜索添加的成员->更新成员信息->搜索更新后的成员->删除该成员
    public void contactManage() throws MalformedURLException, InterruptedException {

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName","android");
        desiredCapabilities.setCapability("deviceName","emulator-5554");
        desiredCapabilities.setCapability("appPackage","com.tencent.wework");
        desiredCapabilities.setCapability("appActivity",".launch.LaunchSplashActivity");
        desiredCapabilities.setCapability("noReset","true");

        driver = new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),desiredCapabilities);

    //添加成员

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击通讯录
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.TextView")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击添加成员
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[5]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击手动输入添加
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout[2]/android.widget.LinearLayout")).click();

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入姓名
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.EditText")).sendKeys("新成员2");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入电话号码
        driver.findElement(MobileBy.id("fow")).sendKeys("188987465432");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //进入地址选择框
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[5]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //手动输入地址
        driver.findElement(MobileBy.id("iz")).sendKeys("123");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击确定
        driver.findElement(MobileBy.id("i6k")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击保存
        driver.findElement(MobileBy.id("i6r")).click();

        Thread.sleep(3000);
        //点击返回到通讯录界面
        driver.findElement(MobileBy.id("i63")).click();


    //搜索成员并更新

        //点击搜索按钮
        driver.findElement(MobileBy.id("i6n")).click();
        //输入搜索内容
        driver.findElement(MobileBy.id("gpg")).sendKeys("新成员2");

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击搜索结果
        driver.findElement(MobileBy.id("dns")).click();
        //点击3竖点按钮
        driver.findElement(MobileBy.id("i6d")).click();
        //点击编辑成员按钮
        driver.findElement(MobileBy.id("b_x")).click();
        //输入新姓名
        driver.findElement(MobileBy.id("b4t")).sendKeys("新成员3");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //进入地址选择框
        driver.findElement(MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[7]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout")).click();
        //输入新地址
        driver.findElement(MobileBy.id("iz")).sendKeys("456");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击确定
        driver.findElement(MobileBy.id("i6k")).click();
        Thread.sleep(3000);
        //点击保存
        driver.findElement(MobileBy.id("i6r")).click();

        Thread.sleep(3000);
        //点击返回按钮
        driver.findElement(MobileBy.id("i63")).click();
        Thread.sleep(3000);
        //再次返回通讯录
        driver.findElement(MobileBy.id("i63")).click();


    //删除成员

        //点击搜索按钮
        driver.findElement(MobileBy.id("i6n")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入搜索内容
        driver.findElement(MobileBy.id("gpg")).sendKeys("新成员3");
        //点击搜索结果
        driver.findElement(MobileBy.id("dns")).click();
        //点击3竖点按钮
        driver.findElement(MobileBy.id("i6d")).click();
        //点击编辑成员按钮
        driver.findElement(MobileBy.id("b_x")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击删除成员按钮
        driver.findElement(MobileBy.id("elh")).click();
        //点击删除确定按钮
        driver.findElement(MobileBy.id("blx")).click();
        Thread.sleep(3000);
        //点击返回按钮
        driver.findElement(MobileBy.id("i63")).click();


    }


}

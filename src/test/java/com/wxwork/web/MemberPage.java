package com.wxwork.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class MemberPage extends BasePage {
    public MemberPage(WebDriver driver) {
        super(driver);
    }

    //po原则6 添加成功与添加失败返回的页面不同，需要封装不同的方法
    public MemberPage addMemberFaile(String username, String acctid, String mobile) {
        return this;
    }
    //po原则6 添加成功与添加失败返回的页面不同，需要封装不同的方法
    public MemberPage addMember(String username, String acctid, String mobile) throws InterruptedException {
        click(By.id("menu_contacts"));
        click(By.linkText("添加成员"));
        Thread.sleep(3000);
        sendKeys(By.name("username"), username);
        sendKeys(By.name("acctid"), acctid);
        sendKeys(By.name("mobile"), mobile);
        click(By.linkText("保存"));
        Thread.sleep(1000);

        return this;

    }

    public MemberPage searchMember(String username){
        //
        return this;
    }

    public MemberPage editMember(String username, String acctid, String mobile){
        //
        return this;
    }

    public MemberPage deleteMember(String username, String mobile) {
        //
        return this;
    }


}
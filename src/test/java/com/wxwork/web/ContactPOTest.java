package com.wxwork.web;

import org.junit.jupiter.api.*;

import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContactPOTest extends BasePage {
    private static MainPage mainPage;
    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        mainPage = new MainPage();
        //删除数据
    }

    @Test
    @Order(1)
    @DisplayName("添加成员")
    public void addMember() throws InterruptedException {

        mainPage.MemberPage().addMember("新成员1","123456","18812345689");

    }



    @Test
    @Order(3)
    @DisplayName("搜索部门")
    public void searchDepart() throws InterruptedException {

        String departname = mainPage.contack().searchDepart("部门1").getPartyInfo();
        Thread.sleep(1000);
        assertTrue(departname.contains("部门无任何成员"));

    }


    @Test
    @Order(2)
    @DisplayName("添加部门")
    public void addDepart() throws InterruptedException {

        String departname = mainPage.contack().addDepart("部门1").getPartyInfo();

        assertTrue(departname.contains("部门1"));
    }

}


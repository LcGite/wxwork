package com.wxwork.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContactClassicTest {

    private static WebDriver driver;
    void click(By by) { driver.findElement(by).click(); }
    void sendKeys(By by, String content) {
        driver.findElement(by).sendKeys(content);
    }

    static void needLogin() throws IOException, InterruptedException {
        //扫码登录
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx");
        driver.manage().window().maximize();
        //sleep 20
        Thread.sleep(10000);
        Set<Cookie> cookies = driver.manage().getCookies();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.writeValue(new File("cookies.yaml"), cookies);

    }

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        File file = new File("cookies.yaml");
        if (file.exists()) {
            //利用cookie复用session登录
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx");
            driver.manage().window().maximize();

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference typeReference = new TypeReference<List<HashMap<String, Object>>>() {
            };

            List<HashMap<String, Object>> cookies = (List<HashMap<String, Object>>) mapper.readValue(file, typeReference);
            System.out.println(cookies);

            cookies.forEach(cookieMap -> {
                driver.manage().addCookie(new Cookie(cookieMap.get("name").toString(), cookieMap.get("value").toString()));
            });

            driver.navigate().refresh();
        } else {
            needLogin();
        }
    }


    @Test
    @Order(1)
    @DisplayName("添加成员")
    void contackAdd() throws InterruptedException {
        click(By.linkText("添加成员"));
        sendKeys(By.name("username"), "新成员1");
        sendKeys(By.name("acctid"), "123456");
        sendKeys(By.name("mobile"), "18812345678");
        click(By.linkText("保存"));
        Thread.sleep(1000);
    }


    @Test
    @Order(3)
    @DisplayName("部门搜索")
    void departSearch() throws InterruptedException {
        click(By.id("menu_contacts"));
        sendKeys(By.id("memberSearchInput"), "部门1");
        String content = driver.findElement(By.cssSelector(".js_party_info")).getText();

        click(By.cssSelector(".ww_icon_AddMember"));
        content = driver.findElement(By.cssSelector(".js_party_info")).getText();

        assertTrue(content.contains("无任何成员"));
        Thread.sleep(1000);
        click(By.id("clearMemberSearchInput"));

    }

    @Test
    @Order(2)
    @DisplayName("添加部门")
    void addDepart() throws InterruptedException {
        click(By.id("menu_contacts"));
        click(By.linkText("添加"));
        click(By.linkText("添加部门"));
        sendKeys(By.name("name"),"部门1");
        click(By.linkText("选择所属部门"));
        Thread.sleep(2000);
        click(By.xpath("//a[@class='jstree-anchor']"));
        click(By.linkText("确定"));
        Thread.sleep(1000);

    }


}

package com.wxwork.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;


public class DepartPage extends BasePage{
    By partyinfo = By.cssSelector(".js_party_info");
    public DepartPage(WebDriver driver) {
        super(driver);
    }


    //添加部门
    public DepartPage addDepart(String departname) throws InterruptedException {
        click(By.id("menu_contacts"));
        Thread.sleep(3000);
        click(By.linkText("添加"));
        click(By.className("js_create_party"));
        sendKeys(By.name("name"),departname);
        click(By.linkText("选择所属部门"));
        Thread.sleep(2000);
        click(By.xpath("//a[@class='jstree-anchor']"));
        click(By.linkText("确定"));
        Thread.sleep(1000);

        return this;
    }


    //搜索部门
    //po原则5 不需要实现所有的方法，按需封装
    public DepartPage searchDepart(String departName) throws InterruptedException {
        //po原则1  用公共方法代表页面所提供的功能
        //po原则3 通常不要在po方法内添加断言

        click(By.id("menu_contacts"));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        sendKeys(By.id("memberSearchInput"),departName);
        click(By.cssSelector(".ww_icon_AddMember"));
        Thread.sleep(3000);

//        click(By.id("clearMemberSearchInput"));
        return this;
    }

    public String getPartyInfo(){
        String PartyInfo = driver.findElement(partyinfo).getText();
        return PartyInfo;
    }

}

